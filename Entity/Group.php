<?php
/**
 * Created by PhpStorm.
 * User: antoineperso
 * Date: 12/02/2016
 * Time: 16:48
 */

namespace jokari4242\UserBundle\Entity;


use FOS\UserBundle\Model\Group as BaseGroup;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_group")
 */
class Group extends BaseGroup
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    public function __toString()
    {
        return parent::getName();
    }
}

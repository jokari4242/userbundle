<?php
/**
 * Created by PhpStorm.
 * User: antoineperso
 * Date: 21/04/2016
 * Time: 13:14
 */

namespace jokari4242\UserBundle\Auth;

use jokari4242\UserBundle\Entity\User;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider as BaseFOSUBProvider;
use Symfony\Component\Security\Core\User\UserInterface;

class OAuthProvider extends BaseFOSUBProvider
{
    /**
     * {@inheritDoc}
     */
    public function connect(UserInterface $user, UserResponseInterface $response)
    {
        // get property from provider configuration by provider name
        // , it will return `facebook_id` in that case (see service definition below)
        $property = $this->getProperty($response);
        $username = $response->getUsername(); // get the unique user identifier

        //we "disconnect" previously connected users
        $existingUser = $this->userManager->findUserBy(array($property => $username));
        if (null !== $existingUser) {
            // set current user id and token to null for disconnect
            // ...

            $this->userManager->updateUser($existingUser);
        }
        //we connect current user, set current user id and token
        // ...
        $this->userManager->updateUser($user);
    }


    private function get_raw_facebook_avatar_url($uid)
    {
        $array = get_headers('https://graph.facebook.com/'.$uid.'/picture?type=large', 1);
        return (isset($array['Location']) ? $array['Location'] : FALSE);
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $userEmail = $response->getEmail();
        $user = $this->userManager->findUserByEmail($userEmail);

        // if null just create new user and set it properties
        if (null === $user) {
            $username = $response->getEmail();
            $email = $response->getEmail();
            $firstname = $response->getFirstName();
            $lastname = $response->getLastName();
            $facebookId = $response->getResponse()['id'];
            $profile = $this->get_raw_facebook_avatar_url($facebookId);
            $token = $response->getTokenSecret();

            $user = new User();
            $user->setUsername($username);
            $user->setEmail($email);
            $user->setFirstname($firstname);
            $user->setLastname($lastname);
            $user->setProfilePicture($profile);
            $user->setPlainPassword(md5(uniqid()));
            $user->setEnabled(1);
            $user->setFacebookAccessToken($token);
            $user->setFacebookId($facebookId);

            // ... save user to database
            $this->userManager->updateUser($user);

            return $user;
        }
        // else update access token of existing user
        $serviceName = $response->getResourceOwner()->getName();
        $setter = 'set' . ucfirst($serviceName) . 'AccessToken';
        $user->$setter($response->getAccessToken());//update access token

        return $user;
    }
}
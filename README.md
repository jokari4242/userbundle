AMUserBundle
============

A extention of user bundle created on February 20, 2016.

Bundle need HWIOAuthBundle for facebook authentification

## Install the bundle


In composer.json
```
    "repositories": [
      {
        "type": "vcs",
        "url": "/Users/antoineperso/Site/AMUserBundle/src/jokari4242/UserBundle"
      }
    ],

"jokari4242/user-bundle": "dev-master"
```

in AppKernel.php
```yml
            new HWI\Bundle\OAuthBundle\HWIOAuthBundle(),
            new FOS\UserBundle\FOSUserBundle(),
            new Scheb\TwoFactorBundle\SchebTwoFactorBundle(),
            new jokari4242\UserBundle\jokari4242UserBundle(),
            new JMS\SerializerBundle\JMSSerializerBundle(),
            
            Only For the dev
            $bundles[] = new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle();
            $bundles[] = new Hautelook\AliceBundle\HautelookAliceBundle();
```


in config.yml
```yml

fos_user:
    db_driver: orm # other valid values are 'mongodb', 'couchdb' and 'propel'
    firewall_name: api
    user_class: jokari4242\UserBundle\Entity\User
    registration:
        form:
            type: jokari4242\UserBundle\Form\Type\RegistrationFormType
    group:
        group_class: jokari4242\UserBundle\Entity\Group


hwi_oauth:
    # list of names of the firewalls in which this bundle is active, this setting MUST be set
    firewall_names: [main]

    # an optional setting to configure a query string parameter which can be used to redirect
    # the user after authentication, e.g. /connect/facebook?_destination=/my/destination will
    # redirect the user to /my/destination after facebook authenticates them.  If this is not
    # set then the user will be redirected to the original resource that they requested, or
    # the base address if no resource was requested.  This is similar to the behaviour of
    # [target_path_parameter for form login](http://symfony.com/doc/2.0/cookbook/security/form_login.html).
    # target_path_parameter: _destination

    # an optional setting to use the HTTP REFERER header to be used in case no
    # previous URL was stored in the session (i.e. no resource was requested).
    # This is similar to the behaviour of
    # [using the referring URL for form login](http://symfony.com/doc/2.0/cookbook/security/form_login.html#using-the-referring-url).
    # use_referer: true

    # here you will add one (or more) configurations for resource owners
    # and other settings you want to adjust in this bundle, just checkout the list below!
    resource_owners:
        facebook:
            type:                facebook
            client_id:           "%facebook_app_id%"
            client_secret:       "%facebook_app_secret%"
#            scope:         "email public_profile user_location user_birthday"
            infos_url:     "https://graph.facebook.com/me?fields=id,name,email,picture.type(square),first_name,last_name,birthday,link"
            options:
#                display: popup #dialog is optimized for popup window
                auth_type: rerequest # Re-asking for Declined Permissions
                
scheb_two_factor:

    # Trusted computer feature
    trusted_computer:
        enabled: false                 # If the trusted computer feature should be enabled
        cookie_name: trusted_computer  # Name of the trusted computer cookie
        cookie_lifetime: 5184000       # Lifetime of the trusted computer cookie

    # Regex pattern of paths you want to exclude from two-factor authentication.
    # Useful to exclude Assetic paths or other files from being blocked.
    # Example: ^/(css|js|images)/
    exclude_pattern: ~

    # POST/GET parameter names
    parameter_names:
        auth_code: _auth_code          # Name of the parameter containing the authentication code
        trusted: _trusted              # Name of the parameter containing the trusted flag

    # Email authentication config
    email:
        enabled: true                  # If email authentication should be enabled, default false
        mailer: my_mailer_service      # Use alternative service to send the authentication code
        sender_email: me@example.com   # Sender email address
        sender_name: John Doe          # Sender name
        digits: 4                      # Number of digits in authentication code
        template: AcmeDemoBundle:Authentication:form.html.twig   # Template used to render the authentication form

    # Google Authenticator config
    google:
        enabled: true                  # If Google Authenticator should be enabled, default false
        server_name: Server Name       # Server name used in QR code
        issuer: Issuer Name            # Issuer name used in QR code
        template: AcmeDemoBundle:Authentication:form.html.twig   # Template used to render the authentication form

    # The service which is used to persist data in the user object. By default Doctrine is used. If your entity is
    # managed by something else (e.g. an API), you have to implement a custom persister
    persister: scheb_two_factor.persister.doctrine

    # If your Doctrine user object is managed by a model manager, which is not the default one, you have to
    # set this option. Name of entity manager or null, which uses the default one.
    model_manager_name: ~

    # The security token classes, which trigger two-factor authentication.
    # By default the bundle only reacts to Symfony's username+password authentication. If you want to enable
    # two-factor authentication for other authentication methods, add their security token classes.
    security_tokens:
        - Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken

    # A list of IP addresses, which will not trigger two-factor authentication
    ip_whitelist:
        - 127.0.0.1
```


in app/config/security.yml
```yml
security:
    access_decision_manager:
        strategy: unanimous
        
    encoders:
        FOS\UserBundle\Model\UserInterface: bcrypt

    role_hierarchy:
        ROLE_ADMIN:       [ ROLE_USER ]
        ROLE_SUPER_ADMIN: [ ROLE_ADMIN ]

    providers:
        hwi:
            id: hwi_oauth.user.provider
        fos_userbundle:
            id: fos_user.user_provider.username_email

    firewalls:
        main:
            pattern: ^/
            oauth:
                resource_owners:
                    facebook:      /login-facebook
                login_path:        /login
                failure_path:      /login
                oauth_user_provider:
                    service: jokari4242_user.oauth_user_provider
            form_login:
                provider: fos_userbundle
                csrf_token_generator: security.csrf.token_manager
                # if you are using Symfony < 2.8, use the following config instead:
                # csrf_provider: form.csrf_provider
            logout:       true
            anonymous:    true
        oauth_token:                                   # Everyone can access the access token URL.
            pattern: ^/oauth/v2/token
            security: false

    access_control:
        - { path: ^/login$, role: IS_AUTHENTICATED_ANONYMOUSLY }
        - { path: ^/register, role: IS_AUTHENTICATED_ANONYMOUSLY }
        - { path: ^/resetting, role: IS_AUTHENTICATED_ANONYMOUSLY }
        - { path: ^/admin/, role: ROLE_ADMIN }
```

## Update database
```yml
php app/console d:s:u --force
```

## Load data fixtures
php app/console h:d:f:l

## Set up the bundle

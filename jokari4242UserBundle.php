<?php

namespace jokari4242\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class jokari4242UserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}

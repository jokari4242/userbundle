<?php
/**
 * Created by PhpStorm.
 * User: antoineperso
 * Date: 04/03/2016
 * Time: 15:10
 */

namespace jokari4242\UserBundle\Services;

use jokari4242\UserBundle\Entity\User;
use Jhg\NexmoBundle\Managers\SmsManager;
use Scheb\TwoFactorBundle\Model\Email\TwoFactorInterface;
use Scheb\TwoFactorBundle\Mailer\AuthCodeMailerInterface;

class SmsMailer implements AuthCodeMailerInterface
{
    private $smsSender;
    private $senderMail;
    private $mailer;
    private $isSmsDisabled;
    private $deliveryPhoneNumber;
    private $senderAddress;

    public function __construct(SmsManager $smsSender, \Swift_Mailer $mailer, $isSmsDisabled, $deliveryPhoneNumber, $senderAddress)
    {
        $this->smsSender = $smsSender;
        $this->mailer = $mailer;
        $this->isSmsDisabled = $isSmsDisabled;
        $this->deliveryPhoneNumber = $deliveryPhoneNumber;
        $this->senderAddress = $senderAddress;
    }

    public function sendAuthCode(TwoFactorInterface $user)
    {
        $msg = "Your validation code is " . $user->getEmailAuthCode();

        $fromName = "SMSAuth";

        $this->sendSMS($user, $msg, $fromName);
    }

    public function sendSMS(User $user, $msg, $fromName)
    {

        // Fallback to mail if isSmsDisabled
        if ($this->isSmsDisabled) {
            $this->sendMail($user->getEmail(), $msg, $fromName);
        } else {

            if ($this->deliveryPhoneNumber !== null) {
                $number = $this->deliveryPhoneNumber;
            } else {
                $number = $user->getPhoneNumber();
            }

            $this->smsSender->sendText($number, $msg, $fromName);
        }
    }

    public function sendMail($deliveryAddress, $msg, $fromName)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject("[SMS - ".$fromName."]")
            ->setFrom($this->senderAddress)
            ->setTo($deliveryAddress);
        $message->setBody($msg, 'text/html');

        return $this->mailer->send($message);
    }
}